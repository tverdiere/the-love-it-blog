import { Component } from "@angular/core";
import { resolve } from "q";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  posts = [
    {
      title: "Mon premier Post",
      content:
        "hpruihrpuiovruiovrucpjvrpovjrpovjrpovjrpiovhvuirvirv " +
        "vruovhjriovhriovhrhvirovhruiovhruiovhriovhyuihryvuiohrvuiohrvuirv",
      loveIts: "",
      created_at: new Date("December 17, 2017 03:24:00")
    },
    {
      title: "Mon second Post",
      content:
        "hpruihrpuiovruiovrucpjvrpovjrpovjrpovjrpiovhvuirvirv " +
        "vruovhjriovhriovhrhvirovhruiovhruiovhriovhyuihryvuiohrvuiohrvuirv",
      loveIts: "",
      created_at: new Date("November 17, 2015 08:14:00")
    },
    {
      title: "Mon troisième post",
      content: "hpruihrpuiovruiovrucpuiovurivjnriovnriouioencuio",
      loveIts: "",
      created_at: new Date("October 25, 2014 12:15:00")
    }
  ];
}
