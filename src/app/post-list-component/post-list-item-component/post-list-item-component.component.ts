import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-post-list-item-component",
  templateUrl: "./post-list-item-component.component.html",
  styleUrls: ["./post-list-item-component.component.scss"]
})
export class PostListItemComponentComponent implements OnInit {
  @Input() title: string;
  @Input() content: string;
  @Input() created_at: Date;

  loveIt: number = 0;
  dontLoveIt: number = 0;
  loveIts: number;

  constructor() {}

  onLoveIt() {
    this.loveIt++;
    this.countLove();
  }

  onDontLoveIt() {
    this.dontLoveIt++;
    this.countLove();
  }

  countLove() {
    this.loveIts = this.loveIt - this.dontLoveIt;
  }

  getColor() {
    if (this.loveIts > 0) {
      return "green";
    } else if (this.loveIts < 0) {
      return "red";
    }
  }

  ngOnInit() {}
}
