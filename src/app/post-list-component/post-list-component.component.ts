import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-post-list-component",
  templateUrl: "./post-list-component.component.html",
  styleUrls: ["./post-list-component.component.scss"]
})
export class PostListComponentComponent implements OnInit {
  @Input() title: string;
  @Input() content: string;
  @Input() created_at: Date;
  @Input() loveIts: number;

  constructor() {}

  ngOnInit() {}
}
